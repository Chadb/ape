#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os
import os.path  # @UnusedImport (pydev bug)
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
import ApeTools
import ApeTools.Build  # @UnusedImport (pydev bug)
call = ApeTools.Build.call
from ApeTools import Config

Config.init()

testFile = "tmp-testCallLogFile.txt"


def setUp():
    if os.path.exists(testFile):
        print >> sys.stderr, "Test file", testFile, "exists, aborting tests"
        sys.exit(1)


def tearDown():
    if os.path.exists(testFile):
        os.remove(testFile)


class testCall(unittest.TestCase):
    def setUp(self):
        setUp()

    def tearDown(self):
        tearDown()

    def testEcho(self):
        s = "hello world"
        call(["echo", s], output=testFile)
        f = open(testFile)
        lines = f.readlines()
        self.assertEqual(len(lines), 3)
        # remember: skip end-of-line
        self.assertEqual(lines[2][:-1], s)

    def testAppend(self):
        s1 = "hello world"
        s2 = "hola mundo"
        call(["echo", s1], output=testFile)
        call(["echo", s2], output=testFile, append=True)
        f = open(testFile)
        lines = f.readlines()
        self.assertEqual(len(lines), 6)
        # remember: skip end-of-line
        self.assertEqual(lines[2][:-1], s1)
        self.assertEqual(lines[5][:-1], s2)

    def testAppendErr(self):
        s1 = "hello world"
        s2 = "hola mundo"
        call(["echo", s1], output=testFile)
        call(["sh", "-c", "echo %s 1>&2" % s2], output=testFile, append=True)
        f = open(testFile)
        lines = f.readlines()
        self.assertEqual(len(lines), 6)
        # remember: skip end-of-line
        self.assertEqual(lines[2][:-1], s1)
        self.assertEqual(lines[5][:-1], s2)

    def testRC0(self):
        rc = call(["sh", "-c", "exit 0"])
        self.assertEqual(rc, 0)

    def testRC1(self):
        rc = call(["sh", "-c", "exit 1"], useException=False)
        self.assertEqual(rc, 1)
        self.assertRaises(ApeTools.InstallError, call, ["sh", "-c", "exit 1"])

    def testNonExistent(self):
        self.assertEqual(call(["this-does-not-exist-I-hope"],
                              useException=False), 1)
        self.assertRaises(ApeTools.InstallError, call,
                          ["this-does-not-exist-I-hope"])


class testEnvironment(unittest.TestCase):
    def setUp(self):
        setUp()
        self.env = dict(os.environ)

    def tearDown(self):
        tearDown()

    def checkEnvOutput(self, tag, value):
        f = open(testFile)
        found = False
        for l in f:
            if l.startswith(tag):
                found = True
                self.assertEquals(l[len(tag):-1], value)
        self.assert_(found)

    def testEnv(self):
        call(["env"], output=testFile)
        self.checkEnvOutput("PATH=", os.environ["PATH"])

    def testBuildEnv(self):
        value = "testCall"
        tag = "TEST_CALL_VAR"
        self.env[tag] = value
        call(["env"], output=testFile, env=self.env)
        self.checkEnvOutput(tag + "=", value)

    def testChangedEnv(self):
        value = "testCallSys"
        tag = "TEST_CALL_VAR"
        os.environ[tag] = value
        call(["env"], output=testFile)
        self.checkEnvOutput(tag + "=", value)
        del os.environ[tag]

if __name__ == '__main__':
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
