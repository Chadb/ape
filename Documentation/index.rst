
User manual and API documentation for the ``Auger Package Environment`` (``ape``)
=================================================================================

This is the documentation for release |release| of ``ape``. Please consult the
`ape trac pages`_ for the documentation of other releases.

The ``Auger Package Environment`` (``ape``) is a tool to install the software
suite of the `Pierre Auger Observatory <http://www.auger.org/>`_. It installs
the `Offline software <http://www.auger.unam.mx/AugerWiki/OfflineSoftware>`_.

The services provided by ``ape`` include:

* Package download.
* Unpacking, building, and installing of packages.
* Setting up of a build environment to compile, test, and execute software
  dependent on the packages installed with ``ape``.
* Status and other reports.

Dependencies between packages are handled during all the processes.

For more information, consult the documentation listed below, especially the
:ref:`user-manual`. We also have `a PDF version <ape.pdf>`_ of the documentation
available.

The development of ``ape`` is hosted at the
`ape trac pages`_.

.. toctree::
   :maxdepth: 2

   manual
   extending
   code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

