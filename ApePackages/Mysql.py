"""
.. _mysql-support:

Build support for mysql connector package
-----------------------------------------

This package mainly for users of old linux distributions, for users of OS X,
or on computing centres where the administrators refuse to install the
mysql client software.

The builder is registered with the name ``mysql``.

.. note::

   If you run a MySQL_ server, on a linux machine, it is strongly recommended
   that you install the client
   headers and libraries from the same source. Otherwise, connecting to
   databases on `localhost` can fail if the path of the unix socket used for
   communication can differ between the server and the client.
"""
import os
import os.path  # @UnusedImport (pydev bug)

from ApeTools import Build
from ApeTools import Config


class Mysql(Build.Package):
    """Install the connector distributed from the `MySQL download site`_."""

    def install(self, logFile, env):
        """Extend standard install to set additional symbolic link
        `%(prefix)/include/mysql -> .` to support Auger Offline's use of
        `#include <mysql/mysql.h>`.
        """
        Build.Package.install(self, logFile, env)
        mysqlIncludeDir = os.path.join(self.prefix, 'include', 'mysql')
        if not (os.path.exists(mysqlIncludeDir) or
                Config.getboolean("ape", "dryRun")):
            os.symlink(".", mysqlIncludeDir)
